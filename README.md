# Packer Debian Template for Proxmox

Packer configuration for creating Debian virtual machine templates for Proxmox VE. Inspired by, and based upon the work started by, 
- [Roman Tomjak](https://github.com/romantomjak/packer-proxmox-template), 
- [N7KnightOne](https://github.com/N7KnightOne/packer-template-debian-11), 
- [Aracloud](https://github.com/aracloud/packer-template-debian-12),
- [lkubb](https://github.com/lkubb/packer-proxmox-templates)

## Requirements/tested with

- [Packer](https://www.packer.io/downloads) 1.8.0+
- [Proxmox VE](https://www.proxmox.com/en/proxmox-ve) 8.1.x
- from proxmox access to the localhost (for provisioning _preseed_) or a reachable http server (gitlab appliance).

## Preparation

### PVE User account

You will need a dedicated user account for Packer.
The following commands add one with the required privileges [[Source](https://github.com/hashicorp/packer/issues/8463#issuecomment-726844945)]:

```bash
pveum useradd packer@pve
pveum passwd packer@pve
pveum roleadd Packer -privs "VM.Config.Disk VM.Config.CPU VM.Config.Cloudinit VM.Config.Memory Datastore.AllocateSpace Sys.Audit Sys.Modify VM.Config.Options VM.Allocate VM.Audit VM.Console VM.Config.CDROM VM.Config.Network VM.PowerMgmt VM.Config.HWType VM.Monitor Datastore.AllocateTemplate SDN.Use"
pveum aclmod / -user packer@pve -role Packer
```

To upload ISO images, `Datastore.AllocateTemplate` privilege might be needed as well.

Proxmox 8.x may require you to add the `SDN.Use` privilege as well.

### PVE API Key

You can add an API key for this user as well. Suppose the key's label is `packer`, `pm_api_username` will be `packer@pve!packer`.

## Creating a new VM Template

Templates are created by converting an existing VM to a template. As soon as the VM is converted, it cannot be started anymore. If you want to modify an existing template, you need to create a new template.

Here's how:

Variables from the `debian.source.pkr.hcl` can be overridden like so:

```sh
$ packer init
$ packer build -var "proxmox_host=10.100.150.10:8006" "proxmox_api_user=<<NOTAREALUSER>>" "proxmox_api_password=<<NOTAREALPASSWORD>>" .
```

or you can just as easily specify a file that contains the variables and multiple files as well:

```sh
$ packer init
$ packer build -var-file vars/debian.pkrvars.hcl .
```
if an existing template exists with the same ID, use the `--force` parameter
```sh
$ packer build --force -var-file vars/debian.pkrvars.hcl .
```

Be sure to adjust this code as necessary for your setup and to make sure *_NOT_* to commit the `.pkrvars.hcl` file that contains your credentials to your git repository.

---

## Deploy a VM from a Template

Right-click the template in Proxmox VE, and select "Clone".

- **full clone** is a complete copy and is fully independent from the original VM or VM Template, but it requires the same disk space as the original
- **linked clone** requires less disk space but cannot run without access to the base VM Template. Not supported with LVM & ISCSI storage types

## Contributing

You can contribute in many ways and not just by changing the code! If you have
any ideas, just open an issue and tell me what you think.

Contributing code-wise - please fork the repository and submit a pull request.

## License

MIT