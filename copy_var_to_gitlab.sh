#!/bin/bash

# Filename containing variables
DEF_VAR_FILE="vars/debian.pkrvars.hcl"

# Prompt user for GitLab instance
read -p "Enter GitLab instance (e.g., gitlab.local.domain): " gitlab_domain

# Set GitLab API endpoint
API_ENDPOINT="https://$gitlab_domain/api/v4/projects/:id/variables"

# Prompt user for GitLab access token
read -s -p "Enter GitLab access token: " access_token
echo

# Prompt user for project name
read -p "Enter project name: " project_name

# Fetch project details to get project ID
project_info=$(curl --header "PRIVATE-TOKEN: $access_token" "https://$gitlab_domain/api/v4/projects?search=$project_name")
project_id=$(echo "$project_info" | jq -r '.[0].id')

# Function to create a new variable
create_variable() {
    local key="$1"
    local value="$2"
    local api_endpoint="$3"
    local access_token="$4"

    # Convert boolean values to lowercase strings
    if [[ "$value" == "true" || "$value" == "false" ]]; then
        value=$(echo "$value" | tr '[:upper:]' '[:lower:]')
    fi

    # Send the variable to the GitLab API
    curl --request POST \
         --header "PRIVATE-TOKEN: $access_token" \
         --form "key=$key" \
         --form "value=$value" \
         "$api_endpoint"
}

# Check if project ID is found
if [ -z "$project_id" ]; then
    echo "Project not found."
    exit 1
fi

# Check if the user provided a different path for the variables file
if [ $# -eq 1 ]; then
    VARIABLES_FILE="$1"
else
    VARIABLES_FILE="$DEF_VAR_FILE"
fi

# Read variables from the file and create each one
while IFS='=' read -r key value; do
    # Add PKR_VAR_ prefix to key
    prefixed_key="PKR_VAR_$key"
    create_variable "$prefixed_key" "$value" "${API_ENDPOINT/:id/$project_id}" "$access_token"
done < "$VARIABLES_FILE"