# -------------------------------------------------------------------------- #
#                           Packer Configuration                             #
# -------------------------------------------------------------------------- #
packer {
  required_plugins {
    proxmox = {
      version = "1.1.3"
      source  = "github.com/hashicorp/proxmox"
    }

    ansible = {
      version = "1.0.3"
      source  = "github.com/hashicorp/ansible"
    }
  }
}
  
# -------------------------------------------------------------------------- #
#                              Local Variables                               #
# -------------------------------------------------------------------------- #
locals { 
    build_version               = formatdate("DD MMM YYYY hh:mm ZZZ", "2018-01-02T23:12:01Z")
    vm_name                     = "${var.os_family}-${var.os_version}"
    template_description        = <<-EOF
                                  ${ local.vm_name } template 
                                  Built on ${ local.build_version } 
                                  ISO_URL: ${ var.iso_url } 
                                  EOF
    default_password            = coalesce(var.default_password, uuidv4())
}

# -------------------------------------------------------------------------- #
#                       Template Source Definitions                          #
# -------------------------------------------------------------------------- #
source "proxmox-iso" "debian-12" {
  proxmox_url                   = "https://${var.pm_host}/api2/json"
  username                      = var.pm_api_username
  password                      = var.pm_api_password
  token                         = var.pm_api_key
  insecure_skip_tls_verify      = false
  node                          = var.pm_node

  boot_command                  = var.boot_command
  boot_wait                     = "10s"

  ssh_username                  = var.default_username
  ssh_password                  = local.default_password
  ssh_timeout                   = var.ssh_timeout

  unmount_iso                   = true
  iso_url                       = var.iso_url
  iso_checksum                  = var.iso_checksum
  iso_storage_pool              = var.iso_storage_pool
  iso_download_pve              = var.iso_download_pve
  
  vm_name                       = "build-${local.vm_name}"
  vm_id                         = var.vm_id
  cores                         = var.cpu_cores
  cpu_type                      = var.cpu_type
  sockets                       = var.cpu_sockets
  os                            = var.os
  memory                        = var.memory
  bios                          = var.bios
  machine                       = var.machine
  scsi_controller               = var.scsi_controller
  qemu_agent                    = true

  http_directory                = "${path.root}/http"
  http_port_min                 = 8100
  http_port_max                 = 8100

  network_adapters {
    bridge                      = var.nic_bridge
    firewall                    = var.nic_firewall
    model                       = var.nic_model
    vlan_tag                    = var.nic_vlan
  }

  disks {
    cache_mode                  = var.disk_cache_mode
    disk_size                   = var.disk_size
    format                      = var.disk_format
    io_thread                   = var.disk_io_thread
    storage_pool                = var.disk_storage_pool
    type                        = var.disk_type
  }

  template_description          = local.template_description
  template_name                 = "template-${local.vm_name}"
}

# -------------------------------------------------------------------------- #
#                             Build Management                               #
# -------------------------------------------------------------------------- #
build {
  sources = [
    "source.file.preseed",
    "source.proxmox-iso.debian-12",
    ]

  # Using ansible playbooks to configure debian
#  provisioner "ansible" {
#    playbook_file              = "./ansible/debian_config.yml"
#    use_proxy                  = false
#    user                       = "${var.default_username}"
#    ansible_env_vars           = ["ANSIBLE_HOST_KEY_CHECKING=False"]
#    extra_arguments            = ["--extra-vars", "ansible_password=${local.default_password}"]
#    only                       = ["proxmox-iso.debian-12"]
#  }
}