# -- Packer Locals variables -- #

variable "os_version" {}

variable "os_family" {
  description = "OS Family builds the paths needed for packer"
  default     = ""
}

# -- Proxmox variables -- #

variable "pm_host" {
  type        = string
  description = "Proxmox API host domain or IP address. Needs to be accessible with https."
}

variable "pm_api_username" {
  type        = string
  description = "Proxmox API username. Example: 'packer@pve', 'root@pam'. When using API key: 'packer@pve!packer_api_key_label'."
}

variable "pm_api_password" {
  type        = string
  sensitive   = true
  default     = null
  description = "Proxmox password for the API user. Either this or pm_api_key is required."
}

variable "pm_api_key" {
  type        = string
  sensitive   = true
  default     = null
  description = "Proxmox API key for the API user. Either this or pm_api_password is required."
}

variable "pm_node" {
  type        = string
  description = "The target node the template will be built on."
}

variable "pm_skip_tls_verify" {
  type        = bool
  default     = false
  description = "Whether to skip validating the API host TLS certificate."
}

# -- General variables -- #

variable "boot_command" {
  type = list(string)
  default = [
  "<esc><wait>",
  "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<enter>",
  "<wait>"
  ]
}

variable "default_username" {
  type        = string
  default     = "packer"
  description = "The name of the default admin user created by preseed."
}

variable "default_password" {
  type        = string
  sensitive   = true
  default     = null
  description = "The name of the default users password."
}

variable "ssh_key" {
  type        = string
  description = "A single SSH pubkey for the default user's authorized_keys. Required since the template will be locked down."
}

variable "ssh_timeout" {
  type        = string
  default     = "15m"
  description = "How long to wait for an SSH connection before cancelling (without QEMU guest agent, this is how long the build can take)."
}

# -- install ISO variables -- #

variable "iso_url" {
  type    = string
  default = "https://get.debian.org/images/release/current/amd64/iso-cd/debian-12.1.0-amd64-netinst.iso"
}

variable "iso_checksum" {
  type    = string
  default = "sha512:9da6ae5b63a72161d0fd4480d0f090b250c4f6bf421474e4776e82eea5cb3143bf8936bf43244e438e74d581797fe87c7193bbefff19414e33932fe787b1400f"
}

variable "iso_storage_pool" {
  type    = string
}

variable "iso_download_pve" {
  type        = bool
  default     = false
  description = "Let the Proxmox server download the specified `iso_url` instead of uploading it in a second step."
}

# -- Template variables -- #

variable "vm_id" {
  type        = number
  default     = 1000
  description = "The VM ID used for the build VM and the built template."
}

variable "cpu_cores" {
  type        = number
  default     = 2
  description = "Number of CPU cores for the VM."
}

variable "cpu_type" {
  type        = string
  default     = "kvm64"
  description = "CPU type to emulate. Best performance: 'host'."
}

variable "cpu_sockets" {
  type        = number
  default     = 1
  description = "Number of CPU sockets for the VM."
}

variable "os" {
  type        = string
  default     = "other"
  description = "Choose the machine OS: 'wxp', 'w2k', 'w2k3', 'w2k8', 'wvista', 'win7', 'win8', 'win10', 'l24', 'l26', 'solaris', 'other'."

  validation {
    condition     = contains(["wxp", "w2k", "w2k3", "w2k8", "wvista", "win7", "win8", "win10", "l24", "l26", "solaris", "other"], var.os)
    error_message = "The OS configuration is invalid."
  }
}

variable "memory" {
  type        = number
  default     = 2048
  description = "Megabytes of memory to associate with the VM."
}

variable "bios" {
  type        = string
  default     = "seabios"
  description = "Choose the machine BIOS: 'ovmf', 'seabios'."

  validation {
    condition     = contains(["ovmf", "seabios"], var.bios)
    error_message = "The bios configuration is invalid."
  }
}

variable "machine" {
  type        = string
  default     = "pc"
  description = "Choose the machine type: 'pc(i440fx)', 'q35'."

  validation {
    condition     = contains(["pc", "q35"], var.machine)
    error_message = "The machine type is invalid."
  }
}

variable "scsi_controller" {
  type        = string
  default     = "virtio-scsi-single"
  description = "The SCSI controller model to emulate: lsi, lsi53c810, virtio-scsi-pci, virtio-scsi-single, megasas, pvscsi."

  validation {
    condition     = contains(["lsi", "lsi53c810", "virtio-scsi-pci", "virtio-scsi-single", "megasas", "pvscsi"], var.scsi_controller)
    error_message = "The SCSI controller model is invalid."
  }
}

# -- Template network variables -- #

variable "nic_bridge" {
  type        = string
  default     = "vmbr0"
  description = "The bridge the default NIC is attached to."
}

variable "nic_firewall" {
  type        = bool
  default     = false
  description = "Whether to enable the PVE firewall for the default NIC."
}

variable "nic_model" {
  type        = string
  default     = "virtio"
  description = "The model of the default NIC."

  validation {
    condition     = contains(["rtl8139", "ne2k_pci", "e1000", "pcnet", "virtio", "ne2k_isa", "i82551", "i82557b", "i82559er", "vmxnet3", "e1000-82540em", "e1000-82544gc", "e1000-82545em"], var.nic_model)
    error_message = "The default NIC model is invalid."
  }
}

variable "nic_vlan" {
  type        = string
  default     = null
  description = "The VLAN tag the default bridge uses. Leave empty for untagged."
}

variable "nic_queues" {
  type        = number
  default     = 0
  description = "Number of packet queues to be used on the default NIC. For routers, reverse proxies or busy HTTP servers. Requires virtio network adapter."
}

# -- Template disk variables -- #

variable "disk_cache_mode" {
  type        = string
  default     = "none"
  description = "How to cache operations to the default disk. Can be 'none', 'writethrough', 'writeback', 'unsafe' or 'directsync'."

  validation {
    condition     = contains(["none", "writethrough", "writeback", "unsafe", "directsync"], var.disk_cache_mode)
    error_message = "The disk cache mode is invalid."
  }
}

variable "disk_size" {
  type        = string
  default     = "5G"
  description = "The disk size of the default disk."
}


variable "disk_format" {
  type        = string
  default     = "raw"
  description = "The format of the default disk: 'raw', 'cow', 'qcow', 'qed', 'qcow2', 'vmdk', 'cloop'."

  validation {
    condition     = contains(["raw", "cow", "qcow", "qed", "qcow2", "vmdk", "cloop"], var.disk_format)
    error_message = "The default disk format is invalid."
  }
}

variable "disk_io_thread" {
  type        = bool
  default     = false
  description = "Create one I/O thread per storage controller, rather than a single thread for all I/O. Requires virtio-scsi-single controller and a scsi or virtio disk."
}

variable "disk_storage_pool" {
  type        = string
  default     = "local-lvm"
  description = "The storage pool for the default disk."
}

variable "disk_type" {
  type        = string
  default     = "virtio"
  description = "The type of the default disk: 'scsi', 'sata', 'virtio', 'ide'."

  validation {
    condition     = contains(["scsi", "sata", "virtio", "ide"], var.disk_type)
    error_message = "The default disk type is invalid."
  }
}

# -- OS settings -- #
variable "locale" {
  type        = string
  default     = "en_US.UTF-8"
  description = "The locale to set."
}

variable "language" {
  type        = string
  default     = "en"
  description = "The system language to set. Two letters, lower case."
}

variable "country" {
  type        = string
  default     = "US"
  description = "The country to set. Two letters, upper case."
}

variable "countries" {
  type        = string
  default     = "Netherlands"
  description = "The country to set for mirror setting. For debian refer to: https://www.debian.org/mirror/list."
}

variable "keymap" {
  type        = string
  default     = "us"
  description = "The X (keyboard) layout to use, see man setxkbmap."
}

variable "timezone" {
  type        = string
  default     = "UTC"
  description = "The timezone to set, like Europe/London."
}