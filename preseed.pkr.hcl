source "file" "preseed" {
  content = templatefile("${path.root}/templates/preseed.cfg.pkr.tpl", {
    default_username    = var.default_username
    default_password    = local.default_password
    locale              = var.locale
    country             = var.country
    countries           = var.countries
    language            = var.language
    timezone            = var.timezone
    keymap              = var.keymap
  })
  target = "${path.root}/http/preseed.cfg"
}