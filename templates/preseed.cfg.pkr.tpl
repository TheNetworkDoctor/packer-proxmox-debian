#_preseed_V1

### Localization
d-i debian-installer/locale string ${locale}
d-i debian-installer/language string ${language}
d-i debian-installer/country string ${country}
d-i keyboard-configuration/xkb-keymap select ${keymap}

### Account setup
d-i passwd/root-login boolean false
d-i passwd/user-fullname string ${default_username}
d-i passwd/user-uid string 1000
d-i passwd/user-password password ${default_password}
d-i passwd/user-password-again password ${default_password}
d-i passwd/username string ${default_username}

### Mirror settings
# If you select ftp, the mirror/country string does not need to be set.
d-i mirror/no-default boolean true
d-i mirror/protocol select http
d-i mirror/http/countries select ${countries}
d-i mirror/http/mirror select deb.debian.org
d-i mirror/http/hostname string deb.debian.org
d-i mirror/http/directory string /debian/

### Clock and time zone setup
d-i clock-setup/utc boolean true
d-i time/zone string ${timezone}
d-i clock-setup/ntp boolean true

### Partitioning
# This makes partman automatically partition without confirmation.
d-i partman-efi/non_efi_system boolean true
d-i partman-auto-lvm/guided_size string max
d-i partman-auto/choose_recipe select atomic
d-i partman-auto/method string lvm
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true
d-i partman-lvm/device_remove_lvm boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true
d-i partman/confirm_write_new_label boolean true

### Package selection
tasksel tasksel/first multiselect standard, ssh-server
# Individual additional packages to install
d-i pkgsel/include string qemu-guest-agent
d-i pkgsel/upgrade select full-upgrade
d-i pkgsel/update-policy select none
d-i pkgsel/updatedb boolean true
popularity-contest popularity-contest/participate boolean false

# This is fairly safe to set, it makes grub install automatically to the UEFI
# partition/boot record if no other operating system is detected on the machine.
d-i grub-installer/only_debian boolean true

# To install to the primary device (assuming it is not a USB stick):
d-i grub-installer/bootdev  string default

### Finishing up the installation
# Avoid that last message about the install being complete.
d-i finish-install/reboot_in_progress note

# Setup passwordless sudo for ${default_username} user variable
d-i preseed/late_command string \
echo "${default_username} ALL=(ALL:ALL) NOPASSWD:ALL" > /target/etc/sudoers.d/${default_username}; chmod 0440 /target/etc/sudoers.d/${default_username}; sed -i '/^deb cdrom:/s/^/#/' /target/etc/apt/sources.list
